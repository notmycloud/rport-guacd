FROM debian:11

COPY --chmod=0755 entrypoint.sh /entrypoint.sh

RUN set -e \
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install \
    curl=7.74.0-1.3+deb11u1 \
    ca-certificates=20210119 \
    libc6=2.31-13+deb11u3 \
    libcairo2=1.16.0-5 \
    libjpeg62-turbo=1:2.0.6-4 \
    libpng16-16=1.6.37-3 \
    libwebp6=0.6.1-2.1 \
    libfreerdp-client2-2=2.3.0+dfsg1-2+deb11u1 \
    libssh2-1=1.9.0-2 \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

ARG RPORT_GUAC_VERSION=1.4.0

# Package Versions https://bitbucket.org/cloudradar/rport-guacamole/downloads/?tab=downloads
# hadolint ignore=DL3003,SC1091
RUN set -e \
    && useradd -d /opt/rport-guacamole/tmp -m -u 1000 -U -r -s /bin/false guac \
    && . /etc/os-release \
    && GUACD_PKG="rport-guacamole_${RPORT_GUAC_VERSION}_${ID}_${VERSION_CODENAME}_$(uname -m).deb" \
    && GUACD_DOWNLOAD="https://bitbucket.org/cloudradar/rport-guacamole/downloads/${GUACD_PKG}" \
    && GUACD_TMP="/tmp/guacd" \
    && mkdir -p "${GUACD_TMP}" \
    && curl -fLSs "${GUACD_DOWNLOAD}" --output "${GUACD_TMP}/rport-guac.deb" \
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install \
    binutils=2.35.2-2 \
    xz-utils=5.2.5-2.1~deb11u1 \
    && (cd "${GUACD_TMP}"; ar x "${GUACD_TMP}/rport-guac.deb") \
    && tar -xvf "${GUACD_TMP}/data.tar.xz" \
    && rm /etc/default/rport-guacamole \
    && apt-get remove -y binutils=2.35.2-2 xz-utils=5.2.5-2.1~deb11u1 \
    && apt-get autoremove -y \
    && apt-get clean && rm -rf /var/lib/apt/lists/* \
    && rm -rf "${GUACD_TMP}" \
    && chown -R guac:guac /opt/rport-guacamole \
    && chmod 0755 /opt/rport-guacamole/sbin/guacd

USER guac

VOLUME [ "/opt/rport-guacamole/tmp" ]

EXPOSE 9445

LABEL guac.server.version=${RPORT_GUAC_VERSION}

ENTRYPOINT [ "sh", "-c", "/entrypoint.sh" ]
