#!/bin/sh

set -e

if /opt/rport-guacamole/sbin/guacd -v;then
  true
else
  echo "Unable to start the RPort Guacamole server."
  exit 1
fi

LD_LIBRARY_PATH=/opt/rport-guacamole/lib exec /opt/rport-guacamole/sbin/guacd -f -b "${RPORT_GUACD_BIND:-0.0.0.0}" -l "${RPORT_GUACD_PORT:-9445}" "$@"

